import React from "react";


export default function Product(props) {
  const { product, onAdd } = props;

  return (
    <div className="col-3 text-center card">
      <img
        src={require(`../images/${product.img}`).default}
        alt={product.name}
      />
      <h3>{product.name}</h3>
      <div>
        {product.price} {product.text}
      </div>
      <div>
        <button onClick={() => onAdd(product)}>Add To Cart</button>
      </div>
    </div>
  );
}
