import React from "react";
import { Link } from "react-router-dom";

export default function Header(props) {
  return (
    <header className="row center">
      <div>
        <Link to="/">
          <h1>Small Shopping Cart</h1>
        </Link>
      </div>
      <div>
        <Link to="/cart/">
          <i class="fas fa-shopping-cart fa-2x py-3"></i>{" "}
          {props.countCartItems ? (
            <button className="badge">{props.countCartItems}</button>
          ) : (
            ""
          )}
        </Link>{" "}
      </div>
    </header>
  );
}
